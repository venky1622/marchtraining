import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';


@Component({
  selector: 'app-day2',
  templateUrl: './day2.component.html',
  styleUrls: ['./day2.component.css']
})
export class Day2Component implements OnInit {

  constructor(private _servie:CommonService) { }

  ngOnInit() {
    this.fetchMyData();
    this.getComments();
  }

  fetchMyData(){
    let mydATAuRL=this._servie.dataUrl
this._servie.getData(mydATAuRL).subscribe(data=>{
  console.log(data);
},error=>{
  console.log(error)
})
  }

  getComments(){
    let myCommentsUrl=this._servie.commentsUrl
    this._servie.getData(myCommentsUrl).subscribe(data=>{
      console.log(data);
    },error=>{
      console.log(error)
    })  
  }
}
