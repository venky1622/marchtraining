import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-day1',
  templateUrl: './day1.component.html',
  styleUrls: ['./day1.component.css']
})
export class Day1Component implements OnInit {

  dataObj:any=[
    {"name": "Hari"},
    {"name": "Prathyusha"},
    {"name": "Rahul"},
    {"name": "Tukaram"},
    {"name": "Hari"},
    {"name": "Prathyusha"},
    {"name": "Rahul"},
    {"name": "Tukaram"},
    {"name": "Hari"},
    {"name": "Prathyusha"},
    {"name": "Rahul"},
    {"name": "Tukaram"},
    {"name": "Hari"},
    {"name": "Prathyusha"},
    {"name": "Rahul"},
    {"name": "Tukaram"}
  ]
  displayData:boolean=false;

  constructor(private route:Router, public _service:CommonService) {
   }

  ngOnInit() {
  }

  displayItems(){
      this.displayData=true;
      if(this.displayData==true){
        console.log("yes it is true")
      }
      else{
        console.log("no it is false")
      }
      console.log(this.displayData)
    }

  
  navigateToDay2(){
  this.route.navigateByUrl("day2");
  }
}
