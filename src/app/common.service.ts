import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from './../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CommonService {


  dataUrl=environment._baseUrl+"posts";
  commentsUrl=environment._baseUrl+"posts/1/comments";

  constructor(private _http:HttpClient) {

   }

   getData(url){
    return this._http.get(url)
  }

}
